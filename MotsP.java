import java.util.ArrayList;


public class MotsP {
	private ArrayList<String> mots = new ArrayList<String>();
	private ArrayList<Integer> poids= new ArrayList<Integer>();
	
	public MotsP(){
	}
	
	public ArrayList<String> getMots() {
		return mots;
	}
	public void setMots(ArrayList<String> motsP) {
		this.mots = motsP;
	}
	public ArrayList<Integer> getPoids() {
		return poids;
	}
	public void setPoids(ArrayList<Integer> poids) {
		this.poids = poids;
	}

	public String getMots(int k){
		return mots.get(k);
	}
	public void setMots(int k, String valeur){
		mots.set(k, valeur);
	}
	public int getPoids(int k){
		return poids.get(k);
	}
	public void setPoids(int k, int valeur){
		poids.set(k, valeur);
	}

	public void ajouter(String valeur){
		int k = mots.indexOf(valeur);
		if (k==-1){
			mots.add(valeur);
			poids.add(1);
		} else {
			poids.set(k,poids.get(k)+1);
		}
	}
	public int size(){
		return mots.size();
	}

	public double getMatches(String[] words){
		int i;
		double resultat = 0;                         //1 si les ensembles sont �gaux
		if ((words!=null) && (mots!=null)){          //0 si les ensembles sont disjoints
			double sommeP = 0;                       //si appartenance(mots�motsP) resultat augmente  proportionnelement � P
			for(i=0;i<mots.size();i++){
				sommeP+=poids.get(i);
				for(String mot : words){
					if (mot.equals(mots.get(i))){
						resultat += poids.get(i);
					}
				}
			}
			resultat=resultat/sommeP;
		}
		return resultat;
	}
	public String toString(){
		return mots.toString()+poids.toString();
	}
}

	