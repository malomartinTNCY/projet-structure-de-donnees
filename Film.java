
public class Film extends Media {
	private int duree=0;

	@Override
	public int getTime() {
		return duree;
	}

	@Override
	public void setTime(int duree) {
		this.duree = duree;
	}
	
	@Override
	public String toString() {
		if (this.getTime()!=0)
		{
			return "Film("+duree+"min) "+tailString();
		}
		else
		{
			return "Film "+tailString();
		}
	}
}
