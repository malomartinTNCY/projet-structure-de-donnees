import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class TestUtilisateur {

Utilisateur utilisateur = new Utilisateur();
	
	@Test
	public void testGetPreferences() {
		utilisateur.setPreferences(3,42);
		assertEquals(42, utilisateur.getPreferences()[3]);
	}

	@Test
	public void testSetGetMedias() {
		utilisateur.setMedias();
		String titre = "Fast & Furious 7";
		String[] realisateurs = {"James Wan"};
		String[] acteurs = {"Vin Diesel", "Paul Walker", "Dwayne Johnson"};
		String[] genres = {"Action", "Crime", "Thriller"};
		int duree = 137;
		assertEquals(titre, utilisateur.getMedias().get(0).getTitre());
		assertArrayEquals(realisateurs, utilisateur.getMedias().get(0).getRealisateurs());
		assertArrayEquals(acteurs, utilisateur.getMedia(0).getActeurs());
		assertArrayEquals(genres, utilisateur.getMedia(0).getGenres());
		assertEquals(duree, utilisateur.getMedia(0).getTime());
	}

	@Test
	public void testSetAllGetActeurs() {
		utilisateur.setMedias();
		utilisateur.getMedias().get(0).setEstVu(true);
		utilisateur.setAll();
		MotsP acteurs = new MotsP();
		ArrayList<String> act = new ArrayList<String>();
		act.add("Vin Diesel");
		act.add("Paul Walker");
		act.add("Dwayne Johnson");
		acteurs.setMots(act);
		ArrayList<Integer> poids = new ArrayList<Integer>();
		poids.add(1);
		poids.add(1);
		poids.add(1);
		acteurs.setPoids(poids);
		assertEquals(acteurs.getMots(), utilisateur.getActeurs().getMots());
		assertEquals(acteurs.getPoids(), utilisateur.getActeurs().getPoids());
	}

	@Test
	public void testSetAllGetRealisateurs() {
		utilisateur.setMedias();
		utilisateur.getMedias().get(0).setEstVu(true);
		utilisateur.setAll();
		MotsP realisateurs = new MotsP();
		ArrayList<String> real = new ArrayList<String>();
		real.add("James Wan");
		realisateurs.setMots(real);
		ArrayList<Integer> poids = new ArrayList<Integer>();
		poids.add(1);
		realisateurs.setPoids(poids);
		assertEquals(realisateurs.getMots(), utilisateur.getRealisateurs().getMots());
		assertEquals(realisateurs.getPoids(), utilisateur.getRealisateurs().getPoids());
	}

	@Test
	public void testSetAllGetGenres() {
		utilisateur.setMedias();
		utilisateur.getMedias().get(0).setEstVu(true);
		utilisateur.setAll();
		MotsP genres = new MotsP();
		ArrayList<String> g = new ArrayList<String>();
		g.add("Action");
		g.add("Crime");
		g.add("Thriller");
		genres.setMots(g);
		ArrayList<Integer> poids = new ArrayList<Integer>();
		poids.add(1);
		poids.add(1);
		poids.add(1);
		genres.setPoids(poids);
		assertEquals(genres.getMots(), utilisateur.getGenres().getMots());
		assertEquals(genres.getPoids(), utilisateur.getGenres().getPoids());
	}

	@Test
	public void testNote() {
		utilisateur.setMedias();
		utilisateur.getMedias().get(0).setEstVu(true);
		utilisateur.getMedias().get(1).setEstVu(true);
		utilisateur.getMedias().get(2).setEstVu(true);
		utilisateur.setAll();
		Media f = utilisateur.getMedias().get(0);		
		assertEquals(1.458,utilisateur.Note(f),0.1);
	}

}
