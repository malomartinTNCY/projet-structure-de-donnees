import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class TestMotsP {
	
	@Test
	public void testSetGetMots() {
		MotsP test = new MotsP();
		ArrayList<String> mots = new ArrayList<String>();
		mots.add("mots1");
		mots.add("mots2");
		mots.add("mots3");
		test.setMots(mots);
		assertEquals(mots,test.getMots());
	}


	@Test
	public void testSetGetMotsInt() {
		MotsP test = new MotsP();
		ArrayList<String> mots = new ArrayList<String>();
		mots.add("mots1");
		mots.add("mots2");
		mots.add("mots3");
		test.setMots(mots);
		mots.set(2,"mots4");
		test.setMots(2,"mot4");
		assertEquals(mots.get(2), test.getMots(2));
	}

	@Test
	public void testSetGetPoids() {
		MotsP test = new MotsP();
		ArrayList<Integer> poids = new ArrayList<Integer>();
		poids.add(1);
		poids.add(1);
		poids.add(1);
		poids.add(3);
		test.setPoids(poids);
		assertEquals(poids,test.getPoids());
	}

	@Test
	public void testSetGetPoidsInt() {
		MotsP test = new MotsP();
		ArrayList<Integer> poids = new ArrayList<Integer>();
		poids.add(1);
		poids.add(1);
		poids.add(1);
		test.setPoids(poids);
		poids.set(2,3);
		test.setPoids(2,3);
		assertEquals((int) poids.get(2),(int) test.getPoids(2));
	}

	
	
	@Test
	public void testAjouter() {
		MotsP test = new MotsP();
		ArrayList<Integer> poids = new ArrayList<Integer>();
		ArrayList<String> mots = new ArrayList<String>();
		poids.add(1);
		poids.add(1);
		test.setPoids(poids);
		mots.add("mots1");
		mots.add("mots2");
		test.setMots(mots);
		
		test.ajouter("mots2");
		test.ajouter("mots3");
		poids.set(1,2);
		poids.add(1);
		mots.add("mots3");
		assertEquals(mots,test.getMots());
		assertEquals(poids,test.getPoids());		
	}

	@Test
	public void testSize() {
		MotsP test = new MotsP();
		test.ajouter("mot1");
		test.ajouter("mot2");
		test.ajouter("mot3");
		test.ajouter("mot4");
		test.ajouter("mot4");
		assertEquals(4, test.size());
	}

	@Test
	public void testGetMatches() {
		MotsP test = new MotsP();
		test.ajouter("mot1");
		test.ajouter("mot1");
		test.ajouter("mot1");
		test.ajouter("mot2");
		test.ajouter("mot2");
		test.ajouter("mot3");
		test.ajouter("mot4");
		String[] Smots1 = {"mot0"};
		String[] Smots2 = {"mot1"};
		String[] Smots3 = {"mot3"};
		String[] Smots4 = {"mot0", "mot1"};
		String[] Smots5 = {"mot1","mot2","mot3"};
		String[] Smots6 = {"mot1","mot2","mot3", "mot4"};
		assertEquals(0.0,test.getMatches(Smots1),0.1);
		assertEquals((double) 3/7,test.getMatches(Smots2),0.1);
		assertEquals((double) 1/7,test.getMatches(Smots3),0.1);
		assertEquals((double) 3/7,test.getMatches(Smots4),0.1);
		assertEquals((double) 6/7,test.getMatches(Smots5),0.1);
		assertEquals((double) 1,test.getMatches(Smots6),0.1);
	}

}
