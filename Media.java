import java.util.Arrays;


public abstract class Media implements Comparable<Media>{
	public Media(){};
	private String titre;
	private String[] realisateurs = null;
	private String[] acteurs= null;
	private String[] genres = null;
	private String synopsis;
	private boolean estVu;
	private double note;
	private int date;
	
	public void setEstVu(boolean estVu) {
		this.estVu = estVu;
	}
	public boolean getEstVu() {
		return estVu;
	}

	public String getSynopsis() {
		return synopsis;
	}
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String[] getRealisateurs() {
		return realisateurs;
	}
	public void setRealisateurs(String[] realisateurs) {
		this.realisateurs = realisateurs;
	}
	public String[] getActeurs() {
		return acteurs;
	}
	public void setActeurs(String[] acteurs) {
		this.acteurs = acteurs;
	}
	public String[] getGenres() {
		return genres;
	}
	public void setGenres(String[] genres) {
		this.genres = genres;
	}
	public double getNote() {
		return note;
	}
	public void setNote(double note){
		this.note=note;
	}
	
	public void setDate(int date){
		this.date=date;
	}
	
	public int getDate(){
		return date;
	}
	public void setTime(int t){
	}
	
	public int getTime(){
		return 0;
	}
	public String tailString() {
		String resultat =titre+ "("+date+")\n\t";
		if (realisateurs != null)
		{
			resultat+="De "+Arrays.toString(realisateurs)+".";
		}
		resultat +=" Avec"+Arrays.toString(acteurs);
		resultat +="\n\t"+synopsis;
		resultat +="\n\t"+Arrays.toString(genres)+note;
		
		return resultat;
	}

	public int compareTo(Media m){ //1 si arg implicite plus grand, 0 si �galit�, -1 sinon
		return (int) ((note-m.getNote())/Math.abs(note-m.getNote()));
	}




}