import java.io.*;
import java.util.Collections;

public class Programme {
	static BufferedReader keyboard=new BufferedReader(new InputStreamReader(System.in));
	static String line = ""; 
	
	public static void main(String[] args)
	{	
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setMedias();
		System.out.println(utilisateur.toString());
		int a;
		while(!line.matches("((.*)\\b\\d\\d?\\b(.*))+")){
			line = ask("Rentrez les id des films que vous avez d�j� vus (id1,id2, ...) : ");
		}
		String[] id = line.split("[^\\d]+");
		for(String s : id){
			if (!s.equals("")){
				a = Integer.parseInt(s);
				if(a < 98){
					utilisateur.getMedias().get(a).setEstVu(true);
				}
			}
		}
		
		utilisateur.setAll();
		
		while(!line.equals("Series") && !line.equals("Films") && !line.equals("Tout")){
			line = ask("Voulez-vous des suggestions uniquement de s�ries ou de films (Series, Films ou Tout) : ");
		}
		switch(line){
		case "Series":
			utilisateur.getPreferences()[3] = 0;
			break;
		case "Films":
			utilisateur.getPreferences()[4] = 0;
			break;
		}
		while(!line.equals("Realisateur") && !line.equals("Genre") && !line.equals("Acteurs")){
			line = ask("Quel est votre crit�re principal (Genre, Realisateur ou Acteurs) : ");
		}

		switch(line){
		case "Genre":
			utilisateur.getPreferences()[2] = 3;
			break;
		case "Realisateur":
			utilisateur.getPreferences()[0] = 3;
			break;
		case "Acteurs":
			utilisateur.getPreferences()[1] = 3;
			break;
		}
		for(Media m : utilisateur.getMedias())
		{
			m.setNote(utilisateur.Note(m));
		}
		Collections.sort(utilisateur.getMedias());
		Collections.reverse(utilisateur.getMedias());
			int i=1;
			int j = 0;
			int l = utilisateur.getMedias().size();
			Media m;
			while((j<7)&&(i<l)){
				m=utilisateur.getMedias().get(i);
				if (!m.getEstVu()){
					if(((m instanceof Film)&&(utilisateur.getPreferences()[3]==1))||((m instanceof Serie)&&(utilisateur.getPreferences()[4]==1))){
						System.out.println(m.toString());
						j++;
					}
				}
				i++;
			}
			
			while(!line.equals("y") && !line.equals("n")){
				ask("Souhaitez-vous plus de suggestions ? (y/n) ");
			}
			if(line.equals("y")){
				j=0;
				while((j<8)&&(i<l)){
					m=utilisateur.getMedias().get(i);
					if(!m.getEstVu()){
						if(((m instanceof Film)&&(utilisateur.getPreferences()[3]==1))||((m instanceof Serie)&&(utilisateur.getPreferences()[4]==1))){
							System.out.println(m.toString());
							j++;
						}
					}
					i++;
				}
				
			}
			
	}
	
	public static String ask(String texte){
		System.out.println("");
		System.out.println(texte);
		try{
			line=keyboard.readLine();
		}catch(IOException e){
			System.out.println("err");
		}
		return line;
	}
}