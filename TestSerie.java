import static org.junit.Assert.*;

import org.junit.Test;


public class TestSerie {

Serie serie = new Serie();
	
	@Test
	public void testSetGetEstVu() {
		serie.setEstVu(true);
		assertEquals(true, serie.getEstVu());
	}

	@Test
	public void testSetGetDuree() {
		serie.setTime(42);
		assertEquals(42, serie.getTime());
	}

	@Test
	public void testSetGetSynopsis() {
		serie.setSynopsis("synopsis");
		assertEquals("synopsis", serie.getSynopsis());
	}

	@Test
	public void testSetGetTitre() {
		serie.setTitre("titre");
		assertEquals("titre", serie.getTitre());
	}

	@Test
	public void testSetGetRealisateurs() {
		String[] realisateurs = {"real"};
		serie.setRealisateurs(realisateurs);
		assertArrayEquals(realisateurs, serie.getRealisateurs());
	}

	@Test
	public void testSetGetActeurs() {
		String[] acteurs = {"acteur"};
		serie.setActeurs(acteurs);
		assertArrayEquals(acteurs, serie.getActeurs());
	}
	
	@Test
	public void testSetGetGenres() {
		String[] genres = {"genre"};
		serie.setGenres(genres);
		assertArrayEquals(genres, serie.getGenres());
	}
	
	@Test
	public void testSetGetDate() {
		int date = 2000;
		serie.setDate(date);
		assertEquals(date,serie.getDate());
	}

	@Test
	public void testSetGetNote() {
		serie.setNote(4.2);
		assertEquals(4.2, serie.getNote(), 0.1);
	}

}
	
