import static org.junit.Assert.*;

import org.junit.Test;


public class TestFilm {

Film film = new Film();
	
	@Test
	public void testSetGetEstVu() {
		film.setEstVu(true);
		assertEquals(true, film.getEstVu());
	}

	@Test
	public void testSetGetDuree() {
		film.setTime(150);
		assertEquals(150, film.getTime());
	}

	@Test
	public void testSetGetSynopsis() {
		film.setSynopsis("synopsis");
		assertEquals("synopsis", film.getSynopsis());
	}

	@Test
	public void testSetGetTitre() {
		film.setTitre("titre");
		assertEquals("titre", film.getTitre());
	}

	@Test
	public void testSetGetRealisateurs() {
		String[] realisateurs = {"real"};
		film.setRealisateurs(realisateurs);
		assertArrayEquals(realisateurs, film.getRealisateurs());
	}

	@Test
	public void testSetGetActeurs() {
		String[] acteurs = {"acteur"};
		film.setActeurs(acteurs);
		assertArrayEquals(acteurs, film.getActeurs());
	}
	
	@Test
	public void testSetGetGenres() {
		String[] genres = {"genre"};
		film.setGenres(genres);
		assertArrayEquals(genres, film.getGenres());
	}
	
	@Test
	public void testSetGetDate() {
		int date = 2000;
		film.setDate(date);
		assertEquals(date,film.getDate());
	}

	@Test
	public void testSetGetNote() {
		film.setNote(4.2);
		assertEquals(4.2, film.getNote(), 0.1);
	}

}
	
