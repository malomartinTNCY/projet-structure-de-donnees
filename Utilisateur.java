import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;

public class Utilisateur {
	
	public Utilisateur(){}

	private ArrayList<Media> medias = new ArrayList<Media>();
	private MotsP acteurs = new MotsP();
	private MotsP realisateurs = new MotsP();
	private MotsP genres = new MotsP();
	private double nbSeries= 0;
	private double nbFilms;
	private double formatMoyen;
	
	private int[] preferences = {1,1,1,1,1};
	
	public int[] getPreferences(){
		return preferences;
	}
	
	public double getNbFilms(){
		return nbFilms;
	}
	
	public double getNbSeries(){
		return nbSeries;
	}
	
	public void setPreferences(int pos, int valeur){
		preferences[pos]=valeur;
	}
	
 	public void setMedias(){
		Media unMedia;
		String temp;
		try
		{
			FileReader fr = new FileReader("C:/Program Files (x86)/eclipse/workspace/Projet_SD/src/Liste_Films.txt");
			Scanner sc = new Scanner(fr);
			while(sc.hasNextLine())
			{
				
				String ligneActuelle = sc.nextLine();
				if (ligneActuelle.matches("\\d\\d?\\.(.*)"))
				{
					if (ligneActuelle.matches("(.*)TV\\s*Series(.*)")){
						unMedia = new Serie();
					}
					else
					{
						unMedia = new Film();
					}
					temp = ligneActuelle.split("\\d\\d?\\.(\\s+)")[1];
					unMedia.setTitre(temp.split("\\(")[0]);
					temp= temp.split("\\(")[1].split("[\\s+\\)]")[0];
					unMedia.setDate(Integer.parseInt(temp));
					unMedia.setSynopsis("");
					ligneActuelle = sc.nextLine();
					while ((!ligneActuelle.matches("Director :(.*)")) && (!ligneActuelle.matches("With :(.*)")))
					{
						unMedia.setSynopsis(unMedia.getSynopsis().concat(ligneActuelle));
						ligneActuelle = sc.nextLine();
					}
					
					if (ligneActuelle.matches("Director :(.*)"))
					{
					temp = ligneActuelle.split("Director :(\\s+)")[1];
					temp = temp.split("\\s*$")[0];
					unMedia.setRealisateurs(temp.split("(\\s*),(\\s*)"));
					ligneActuelle = sc.nextLine();
					}
										
					temp = ligneActuelle.split("With :(\\s+)")[1];
					temp = temp.split("\\s*$")[0];
					unMedia.setActeurs(temp.split("(\\s*),(\\s*)"));
					
					ligneActuelle = sc.nextLine();
					temp = ligneActuelle.split("\\s\\d")[0];
					unMedia.setGenres(temp.split("(\\s*)\\|(\\s*)"));
					
					if (ligneActuelle.length()>temp.length()+1)
					{
						temp = ligneActuelle.substring(temp.length()+1);
						temp=temp.split("\\s+")[0];
						unMedia.setTime(Integer.parseInt(temp));
					}
					
					medias.add(unMedia);
				}
			}
			sc.close();
		}
		catch(Exception ex)
		{
			System.err.println("Erreur de lecture : "+ ex);
		}
	}

	public ArrayList<Media> getMedias(){
		return medias;
	}
	
	public Media getMedia(int i){
		return medias.get(i);
	}
	
	public String toString() {
		StringBuilder resultat = new StringBuilder("");
		for(int i = 0 ; i < (medias.size()+1)/3-1 ; i++){
			String esp1 = "", esp2 = "";
			for(int j = 0 ; j < 70 - ((medias.get(i)).getTitre()).length() ; j++){
				esp1 = esp1.concat(" ");
			}
			for(int j = 0 ; j < 70 - ((medias.get(i+33)).getTitre()).length() ; j++){
				esp2 = esp2.concat(" ");
			}
			resultat.append(""+i+". "+(medias.get(i)).getTitre()+esp1+(i+33)+". "+(medias.get(i+33)).getTitre()+esp2+(i+66)+". "+(medias.get(i+66)).getTitre());
			resultat.append("\n");
		}
		//resultat.append("format moyen:"+formatMoyen+" ; duree moyenne:"+dureeMoyenne);
		return resultat.toString();
	}

	
	public MotsP getActeurs() {
		return acteurs;
	}

	public MotsP getRealisateurs() {
		return realisateurs;
	}

	public MotsP getGenres() {
		return genres;
	}

	
	public void setAll(){
		int j;
		for(Media m : medias){
			if(m.getEstVu()){
				if (m.getRealisateurs() != null){
					for(j = 0 ; j < m.getRealisateurs().length ; j++){
						realisateurs.ajouter(m.getRealisateurs()[j]);
					}
				}
				for(j = 0 ; j < m.getActeurs().length ; j++){
					acteurs.ajouter(m.getActeurs()[j]);
				}
				for(j = 0 ; j < m.getGenres().length ; j++){
					genres.ajouter(m.getGenres()[j]);
				}
				if (m instanceof Serie){
					nbSeries++;
					formatMoyen+=m.getTime();
				}
				else
				{
					nbFilms++;
				}
			}
		}
		if (nbSeries!=0) formatMoyen = formatMoyen/nbSeries;
	}

	public double Note(Media m){
		double resultat = 0;
		resultat+= realisateurs.getMatches(m.getRealisateurs())*preferences[0];
		if (m instanceof Serie)
		{
			resultat/=2;
			if (formatMoyen !=0) resultat+=(1-(Math.abs(formatMoyen-m.getTime())/60));
		}
		resultat+= acteurs.getMatches(m.getActeurs())*preferences[1];
		resultat+= genres.getMatches(m.getGenres())*preferences[2];
		if (m instanceof Film)
		{
			resultat*=(1+nbFilms/(nbFilms+nbSeries)/2);
		}
		else
		{
			resultat*=(1+nbSeries/(nbFilms+nbSeries)/2);
		}
		return resultat;
	}

}
