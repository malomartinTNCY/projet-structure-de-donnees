
public class Serie extends Media {
	private int format=0;

	@Override
	public int getTime() {
		return format;
	}

	@Override
	public void setTime(int format) {
		this.format = format;
	}
	
	@Override
	public String toString() {
		if (this.getTime()!=0)
		{
			return "Serie("+format+"min) "+tailString();
		}
		else
		{
			return "Serie "+tailString();
		}
	}
}
